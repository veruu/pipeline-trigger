"""Fake classes."""
# pylint: disable=too-few-public-methods

from unittest import mock


def load_triggers(gitlab_instance, config, kickstart):
    # pylint: disable=unused-argument
    """Fake load_triggers function."""
    return [{'trigger': 'fake'}]


class Event():
    """Dummy event for proton."""

    def __init__(self, *args, **kwargs):
        """Initialize."""


class FakeConnection():
    """Fake connection."""

    def __init__(self):
        """Initialize."""
        self._headers = {}

    def putheader(self, key, value):
        """Add a fake header."""
        self._headers[key] = value

    def get_headers(self):
        """Return the headers."""
        return self._headers


class WrapMe():
    """The weird XMLRPC interface that eats version first."""

    def __init__(self):
        """Initialize."""

    def test_me(self, version, *_, **__):
        # pylint: disable=no-self-use
        """Return a fake response."""
        return (version, 'value')


class FakeServerProxy():
    """Fake server proxy."""

    def __init__(self):
        """Initialize."""
        self._projects = []
        self._patches = []

    def add_patch(self, data):
        """Add a fake patch."""
        self._patches.append(data)

    def add_project(self, data):
        """Add a fake project."""
        self._projects.append(data)

    @staticmethod
    def __filter_patch(patch, project_id, patch_id):
        if not project_id:
            return patch['id'] > patch_id
        if not patch_id:
            return patch['project_id'] == project_id

        return patch['project_id'] == project_id and patch['id'] > patch_id

    def project_list(self, project_name):
        """Fake a project list."""
        return list(filter(lambda project: project_name in project['linkname'],
                           self._projects))

    def patch_list(self, filters, archived, fields):
        # pylint: disable=unused-argument
        """Fake a patch list."""
        return list(filter(
            lambda patch: self.__filter_patch(
                patch, filters.get('project_id'), filters.get('id__gt')
            ),
            self._patches
        ))


class FakeGitLabCommits():
    """Fake gitlab commits."""

    def __init__(self):
        """Initialize."""
        self._commits = []

    def create(self, data):
        """Create a fake commit."""
        self._commits.append(data)

    def __getitem__(self, index):
        """Get a fake commit."""
        return self._commits[index]


class FakePipelineJob():
    """Fake pipeline job."""

    def __init__(self, job_id):
        """Initialize."""
        self.attributes = {'id': job_id}


class FakePipelineJobs():
    """Fake pipeline jobs."""

    def __init__(self):
        """Initialize."""
        self._jobs = []
        self.add_job(1)

    def add_job(self, job_id):
        """Add a fake job."""
        self._jobs.append(FakePipelineJob(job_id))

    def list(self, page=None, per_page=None):
        # pylint: disable=unused-argument
        """List all fake jobs."""
        return self._jobs


class FakePipeline():
    """Fake pipeline."""

    def __init__(self, branch, token, variables, status):
        """Initialize."""
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status}
        for key, value in variables.items():
            self.attributes[key] = value

        self.branch = branch
        self.token = token
        self.variables = variables


class FakeProjectPipelines():
    """Fake project pipelines."""

    def __init__(self):
        """Initialize."""
        self._pipelines = []

    def add_new_pipeline(self, branch, token, variables, status):
        """Add a fake pipeline."""
        self._pipelines.append(
            FakePipeline(branch, token, variables, status)
        )

    def __getitem__(self, index):
        """Get a fake pipeline."""
        return self._pipelines[index]

    def list(self, **kwargs):
        """List all fake pipelines."""
        kwargs.pop('as_list')
        if not kwargs:
            return self._pipelines

        to_return = []
        for pipeline in self._pipelines:
            if pipeline.branch == kwargs.get('ref'):
                to_return.append(pipeline)
            elif pipeline.attributes['status'] == kwargs.get('status'):
                to_return.append(pipeline)

        return to_return


class FakeGitLabProject():
    """Fake gitlab project."""

    def __init__(self):
        """Initialize."""
        self.commits = FakeGitLabCommits()
        self.attributes = {'web_url': 'http://web-url'}
        self.pipelines = FakeProjectPipelines()
        self.manager = mock.MagicMock()

    def trigger_pipeline(self, branch, token, variables, status='pending'):
        """Add a fake pipeline."""
        self.pipelines.add_new_pipeline(branch, token, variables, status)


class FakeGitLab():
    """Fake gitlab."""

    def __init__(self):
        """Initialize."""
        self.projects = {}

    def add_project(self, project_name):
        """Add a fake project."""
        self.projects[project_name] = FakeGitLabProject()
