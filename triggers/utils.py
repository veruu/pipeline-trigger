"""Generic functions and constants."""
from dataclasses import dataclass
from dataclasses import field
import email
import hashlib
import re
import subprocess

from cki_lib import gitlab
from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)

# Patch name skip patterns. These patches live in kernel Patchworks but don't
# apply to the tree. Sometimes, a feature requires a change both in kernel and
# a tool closely associated with it. These are sent as a single series and
# maintainers know how to deal with them, but we don't want to download them
# because we'd just get patch application errors since the tools are not part
# of the kernel tree. We also aren't interested in pull requests, which also
# can end up in Patchwork.
PATCH_SKIP_PATTERNS = [r'\[[^\]]*iproute.*?\]',
                       r'\[[^\]]*pktgen.*?\]',
                       r'\[[^\]]*ethtool.*?\]',
                       r'\[[^\]]*git.*?\]',
                       r'\[[^\]]*pull.*?\]',
                       r'pull.?request']
PATCH_SKIP = re.compile('|'.join(PATCH_SKIP_PATTERNS), re.IGNORECASE)


@dataclass
class SeriesData:
    # pylint: disable=too-many-instance-attributes
    """Class for handling patch series data.

    'last_tested' is customizable by the trigger -- PW2 uses event ID while
    PW1 a patch ID. Both are kept as strings as we are not using them in the
    triggers, only passing the values to GitLab API which requires strings.
    """

    patches: list = field(default_factory=list)
    emails: set = field(default_factory=set)
    subject: str = ''
    message_id: str = ''
    last_tested: str = ''
    series_id: str = ''
    cover: str = ''
    submitter: str = ''

    def __repr__(self):
        """Return readable representation of the series."""
        return '{} {}\n{} {}\n{} {}\n{} {}\n{} {}\n{} {}\n{} {}\n{} {}'.format(
            'Data for series:', self.series_id,
            'Message ID:', self.message_id,
            'Subject:', self.subject,
            'Cover letter:', self.cover,
            'Patches:', self.patches,
            'Last tested:', self.last_tested,
            'Retrieved emails:', self.emails,
            'Submitter: ', self.submitter
        )


def generate_ci_hash(project, pr_id, comment_id):
    """Generate a hash for CI pipeline, used as cki_pipeline_id.

    This is used for GitLab repositories that need to be tested internally.

    Args:
        project:    Slug of the project being tested.
        pr_id:      ID of the merge/pull request.
        comment_id: ID of the comment that triggered testing.

    Returns:
        A sha256 digest of formatted string of arguments.
    """
    to_format = f'{project}@{pr_id}:{comment_id}'
    return hashlib.sha256(to_format.encode('utf-8')).hexdigest()


def get_commit_hash(repository, git_ref):
    """Return commit hash for the ref in question.

    The reference needs to include the specifier, e.g. use 'refs/heads/master'
    instead of just 'master'; to avoid collisions.

    Args:
        repository: Git repository URL.
        git_ref:    Git reference to get commit hash for.

    Returns:
        String representing the commit hash or None if an error occurred.
    """
    LOGGER.debug('Getting the last commit from %s@%s', git_ref, repository)
    try:
        lines = subprocess.check_output(
            ['git', 'ls-remote', repository],
            timeout=60
        ).decode('utf-8').split('\n')
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Unable to list remote %s', repository)
        return None
    matches = [line for line in lines if line.endswith(git_ref)]
    if not matches:
        LOGGER.warning('Ref %s not found in remote %s', git_ref, repository)
        return None
    return matches[0].split()[0]


def was_tested(project, branch, pipeline_id):
    """Find out if the pipeline was already tested or not.

    Args:
        project:     GitLab project which the pipelines are associated with.
        branch:      Project's branch responsible for testing of the repo.
        pipeline_id: ID of the pipeline we want to check.

    Returns:
        True if the pipeline with given pipeline_id was already tested, False
        otherwise.
    """
    for pipeline in project.pipelines.list(as_list=False, ref=branch):
        variables = gitlab.get_variables(pipeline)
        if variables.get('cki_pipeline_id') == pipeline_id:
            return True

    return False


def get_emails_from_headers(headers):
    """Retrieve From, To and Cc header values for given patch.

    Args:
        headers: A list of patch's email headers, grabbed from Patchwork's API
                 response. patch['headers']

    Returns:
        A set of sanitized and decoded unique email addresses.
    """
    email_headers = ['From', 'To', 'Cc']
    emails = set()

    for key in email_headers:
        if key in headers:
            unfolded = re.sub(r'\r?\n[ \t]', ' ', headers[key])
            email_data = email.utils.getaddresses([unfolded])
            for _, address in email_data:
                emails.add(address)

    return emails
